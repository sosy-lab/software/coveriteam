# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "CoVeriTeam"
dynamic = ["version"]
description = "CoVeriTeam: On-Demand Composition of Cooperative Verification Systems"
readme = "README.md"
license = "Apache-2.0"
authors = [{ name = "Sudeep Kanav" }, { name = "Henrik Wachowitz" }]
keywords = ["cooperative", "validation", "verification"]
classifiers = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: POSIX :: Linux",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Topic :: Software Development :: Testing",
]
dependencies = [
    "antlr4-python3-runtime>=4.8",
    "benchexec>=3.24",
    "coloredlogs",
    "requests>=2.24",
    "tqdm",
    "fm-tools>=0.3.3",
    "httpx",
    "setuptools",
]

[project.optional-dependencies]
server = ["pyrate-limiter"]

[tool.ruff]
line-length = 120
exclude = ["fm-tools", "coveriteam/parser"]

[tool.ruff.lint.pydocstyle]
convention = "google"

[tool.ruff.lint]
ignore = ["E402"]
select = [
    # pycodestyle
    "E",
    # Pyflakes
    "F",
    # flake8-bugbear
    "B",
    # isort
    "I",
]

[tool.hatch.build.targets.wheel]
packages = ["coveriteam"]
artifacts = ["*.prp"]


[project.scripts]
coveriteam = "coveriteam.coveriteam:main"

[project.urls]
Homepage = "https://gitlab.com/sosy-lab/software/coveriteam"

[tool.hatch.version]
path = "coveriteam/__init__.py"

[tool.hatch.build.targets.sdist]
include = ["/coveriteam"]

[tool.hatch.envs.test]
dependencies = [
    "coverage[toml]",
    "pytest",
    "pytest-cov",
    "pytest-mock",
    "pytest-subprocess",
]

scripts.cov = "pytest --cov=coveriteam --cov-report=term -k 'not test_portfolio and not test_cCegar and not test_validity_of_download_url' test/"
scripts.cov-exhaustive = "pytest -k 'not test_validity_of_download_url' --cov=coveriteam --cov-report=term test/"
scripts.urls = "pytest test/test_download_url.py"

[[tool.hatch.envs.test.matrix]]
python = ["3.10", "3.12"]
