// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// CPAchecker

// Create verifier from external-actor definition file
cpachecker = ActorFactory.create(ProgramVerifierWithHTMLReport, "../fm-tools/data/cpachecker.yml", version);

// Print type information of the created ProgramVerifier
print(cpachecker);

// Prepare example inputs
prog = ArtifactFactory.create(CProgram, program_path, data_model);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
inputs = {'program':prog, 'spec':spec};

// Execute the verifier on the inputs
result = execute(cpachecker, inputs);
print(result);
