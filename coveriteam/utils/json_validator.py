# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from typing import Any, Dict, Type


class ValidationError(Exception):
    pass


class KeyValidationError(ValidationError):
    def __init__(self, key: Any, keyT: Type, *args: object) -> None:
        self.key = key
        self.keyT = keyT
        super().__init__(*args)

    def __repr__(self):
        return f"Key {self.key} is not of type {self.keyT.__name__}"

    def __str__(self):
        return self.__repr__()


class ValueValidationError(ValidationError):
    def __init__(self, value: Any, valueT: Type, *args: object) -> None:
        self.value = value
        self.valueT = valueT
        super().__init__(*args)

    def __repr__(self):
        return f"Value {self.value} is not of type {self.valueT.__name__}"

    def __str__(self):
        return self.__repr__()


class FlatMappingValidator:
    """
    Validates that a dict is a flat mapping, i.e., of the form {typeA: typeB}.
    """

    def __init__(self, keyT: Type, valueT: Type):
        self.keyT = keyT
        self.valueT = valueT

    def is_valid(self, data: Dict[Any, Any]):
        for key, value in data.items():
            if not (isinstance(key, self.keyT) and isinstance(value, self.valueT)):
                return False
        return True

    def iter_errors(self, data: Dict[Any, Any]):
        for key, value in data.items():
            if not isinstance(key, self.keyT):
                yield KeyValidationError(key, self.keyT)
            if not isinstance(value, self.valueT):
                yield ValueValidationError(value, self.valueT)
