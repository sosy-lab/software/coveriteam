# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import json
import logging
from pathlib import Path
from typing import Any, Dict, List, Union

import benchexec
import yaml
from fm_tools import FmData
from fm_tools.download import DownloadDelegate

import coveriteam.util as util
from coveriteam.language import CoVeriLangException
from coveriteam.util import (
    CVT_DEBUG_LEVEL,
    SESSION_PROVIDER,
)


class ActorConfig:
    """
    This class extracts the relevant options from actor configs given as yaml.
    These options are:
        - actor_name
        - reslim
        - version
        - options
        - archive_location
        - environment
        - toolinfo_module
    """

    def get_archive_location(self) -> str:
        return self.archive_location

    def get_cache_path(self) -> Path:
        return self.cache_path

    def get_version(self) -> str:
        return self.version

    def get_options(self) -> List[str]:
        return self.options

    def get_resource_limits(self) -> Dict[str, Union[int, str]]:
        return self.reslim

    def get_actor_name(self) -> str:
        return self.actor_name

    def get_environment(self) -> Dict[str, str]:
        return self.environment

    def __init__(self, path, version=None, resource_limits: Path = None):
        self.path = path
        config = self.read_yaml(path)

        logging.log(CVT_DEBUG_LEVEL, "External resource limits: %s.", resource_limits)
        if resource_limits:
            resource_limits = self.read_yaml(resource_limits)

        format_version = config.get("format_version", None)
        format_version = config.get("fmtools_format_version", format_version)
        self._check_valid_format_version(format_version)

        self.fm_data = FmData(config, version)

        self._config = self.__sanitize_yaml_dict(self.fm_data._config)
        self.actor_name = self.fm_data.get_actor_name()
        self.cache_path = util.get_CACHE_DIR_PATH() / Path("_cache.json")
        self.reslim = self._config.get("resourcelimits", None)
        self.version = self.fm_data.get_version()
        tool_config = self.fm_data._version_specific_config
        self.tool_dir = str(util.get_INSTALL_DIR() / self.actor_name)
        self.options = self.fm_data.get_options()
        if util.UPDATE_CACHE:
            self.archive_location = self.fm_data.get_archive_location().resolve()
        else:
            self.archive_location = None
        self.environment = tool_config.get("environment", None)

        if "resourcelimits" in tool_config:
            self.reslim = tool_config["resourcelimits"]

        if not (resource_limits or self.reslim):
            resource_limits = {
                "memlimit": "8 GB",
                "timelimit": "2 min",
                "cpuCore": "2",
            }
            logging.warning(
                "No resources given for actor %s. Using default values: %s.",
                self.actor_name,
                resource_limits,
            )
        if self.reslim is None:
            self.reslim = self._sanitize_resource_limits(resource_limits)
            logging.log(
                CVT_DEBUG_LEVEL,
                "Using externally provided resource limits: %s.",
                self.reslim,
            )

        if util.UPDATE_CACHE:
            self.update_cache()

        if not version:
            log_version_warnings(self.actor_name)

        self.tool_name = self._resolve_tool_info_module()

    @staticmethod
    def read_yaml(path: Path):
        with open(path, "r") as file:
            try:
                d = yaml.safe_load(file)  # noqa S506
            except yaml.YAMLError as e:
                msg = "Actor config yaml file {} is invalid: {}".format(path, e)
                raise CoVeriLangException(msg, 203) from e

            return d

    @staticmethod
    def _check_valid_format_version(format_version):
        if format_version is None:
            raise CoVeriLangException("The actor config file does not specify a format version.")
        if format_version == "1.2":
            raise CoVeriLangException(
                f"Format version {format_version} is not supported anymore. Please use format version 2.0"
            )
        if format_version == "2.0":
            pass
        else:
            raise CoVeriLangException(f"Unknown format version: {format_version}.")

    @staticmethod
    def _sanitize_resource_limits(resource_limits):
        if not resource_limits:
            return None
        if resource_limits.get("memlimit"):
            resource_limits["memlimit"] = benchexec.util.parse_memory_value(resource_limits.get("memlimit"))
        if resource_limits.get("timelimit"):
            resource_limits["timelimit"] = benchexec.util.parse_timespan_value(resource_limits.get("timelimit"))
        return resource_limits

    @staticmethod
    def __sanitize_yaml_dict(config: Dict[str, Any]):
        reslim = ActorConfig._sanitize_resource_limits(config.get("resourcelimits"))
        if reslim:
            config["resourcelimits"] = reslim

        for archive in config.get("versions", []):
            reslim = ActorConfig._sanitize_resource_limits(archive.get("resourcelimits", None))
            if reslim:
                archive["resourcelimits"] = reslim

        return config

    def update_cache(self):
        cache = self.init_cache()

        if self.actor_name not in cache:
            self.add_new_entry(cache)

        self.download_if_needed(cache)

    def init_cache(self):
        if not self.get_cache_path().is_file():
            with open(self.get_cache_path(), "w") as json_cache:
                json.dump({}, json_cache)

        with open(self.get_cache_path(), "r") as json_cache:
            cache = json.load(json_cache)

        return cache

    def add_new_entry(self, cache):
        cache[self.actor_name] = ""
        with open(self.get_cache_path(), "w") as json_cache:
            json.dump(cache, json_cache)

    def download_if_needed(self, cache):
        cached_checksum = cache[self.actor_name]
        new_checksum = self.fm_data.archive_location.checksum()
        if not new_checksum:
            raise CoVeriLangException("Failed to acquire checksum.")

        if cached_checksum != new_checksum:
            self.fm_data.download_and_install_into(
                Path(self.tool_dir),
                delegate=DownloadDelegate(SESSION_PROVIDER.get_session()),
            )
            self.save_to_cache(new_checksum, cache)

    def save_to_cache(self, new_checksum, cache):
        cache[self.actor_name] = new_checksum
        with open(self.get_cache_path(), "w") as json_cache:
            json.dump(cache, json_cache)

    def _resolve_tool_info_module(self):
        target = util.get_TOOL_INFO_DOWNLOAD_PATH()

        ti = self.fm_data.get_toolinfo_module()
        if util.UPDATE_CACHE:
            ti.resolve(
                target_dir=target,
                delegate=DownloadDelegate(SESSION_PROVIDER.get_session()),
            )

        return str(ti)


def dict_merge(d1, d2):
    # This function recursively merges (updates) the dictionaries.
    for k in d2.keys():
        if k in d1.keys():
            if isinstance(d1[k], dict) and isinstance(d2[k], dict):
                d1[k] = dict_merge(d1[k], d2[k])
            elif isinstance(d1[k], dict) or isinstance(d2[k], dict):
                # TODO this could be and XOR
                # We raise an error when one of the values is a dict, but not the other.
                msg = "YAML file could not be parsed. Clash in the tag: %r" % k
                raise CoVeriLangException(msg, 201)
            d1[k] = d2[k]
        else:
            d1[k] = d2[k]

    return d1


def sanitize_url_string(url: str):
    """
    This function sanitizes the url string by replacing the special characters with a hyphen.
    """
    return url.replace("://", "-").replace("/", "-").replace(":", "-").replace("~", "-")


def log_version_warnings(actor_name):
    logging.warning(
        "No version specified for actor %s. Taking the first version from the actor definition file.",
        actor_name,
    )
    logging.warning("You can specify the tool's version as a third parameter in the call to ActorFactory.create().")
