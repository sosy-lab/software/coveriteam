# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
import logging
import os
import re
import sys
from pathlib import Path
from typing import TYPE_CHECKING, Any, Callable, Dict, List, Type
from zipfile import ZIP_DEFLATED, ZipFile

import requests
from benchexec.cgroups import CGROUPS_V1, _get_cgroup_version

if TYPE_CHECKING:
    from coveriteam.language.artifact import Artifact

try:
    import tomllib as toml
except ImportError:
    import tomli as toml


CVT_DEBUG_LEVEL = 15
PORTFOLIO_USE_MPI = False
CURRENTLY_IN_MPI = False
INPUT_FILE_DIR = f'{Path(__file__).parent.resolve() / "artifactlibrary/"}/'

_SEARCH_ORDER: tuple[Path, ...] = (
    Path(__file__).parent.parent / ".coveriteam_rc",
    Path.cwd() / ".coveriteam_rc",
    Path.home() / ".coveriteam_rc",
    Path.home() / ".config" / "coveriteam" / ".coveriteam_rc",
)

BASE_CONFIG = """
[defaults]
# The output directory is created relative to the current working directory. If modified, give relative path.
log_dir = "cvt-output"
tool_output_file = "output.txt"
verbose = false
trust_tool_info = false         # Trust the tool info modules. This will load them without a container.

# Caching
# Directory to download and install tools. Set either "false" or give full path.
# "false" uses default "/home/.cache/coveriteam" directory.
cache_dir = false
no_cache_update = false

# Network settings
rate_limit = false         # Path to the database file for rate limiting. If "false", rate limiting is disabled.

# Logging settings
# Choices: WARNING, DEBUG, CVT_DEBUG
# WARNING -> Default.
# CVT_DEBUG -> Set the logging to debug for CoVeriTeam. Only logs debug (and higher) messages from CoVeriTeam.
# DEBUG -> Set the logging to debug globally. Also logs debug (and higher) messages from BenchExec.
logger_level = "warning"
"""


class Config:
    """
    A singleton class holding CoVeriTeam's configuration.
    """

    _instance = None
    _config = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    def load(self, args) -> dict[str, Any]:
        if self._config:
            return self._config

        for path in _SEARCH_ORDER:
            if not path.exists():
                continue

            # Configuration is in TOML format
            with path.open("rb") as f:
                self._config = toml.load(f)
                self._config_source = path
                print("Configuration taken from: '%s'" % self._config_source)
                break

        if not self._config:
            print("Configuration file not found, taking default configuration values:\n%s\n" % BASE_CONFIG)
            self._config = toml.loads(BASE_CONFIG)

        self._config["defaults"]["log_dir"] = Path.cwd() / self._config["defaults"]["log_dir"]
        self._overwrite_default_values(args=args)

        return self._config

    def _overwrite_default_values(self, args) -> dict[str, Any]:
        for attr, value in vars(args).items():
            if value or attr not in self._config["defaults"]:
                self._config["defaults"][attr] = value

    def update(self, values_to_update: dict[str, Any]):
        for attr, value in values_to_update.items():
            if attr in self._config["defaults"]:
                self._config["defaults"][attr] = value
            else:
                logging.warning("Option '%s' is not part of the configuration.", attr)


class SessionProvider:
    """
    This class manages which session shall be used for requests.
    """

    def __init__(self):
        self.session = requests.Session()

    def set_rate_limit(self, path):
        from pyrate_limiter import SQLiteBucket  # noqa: F811
        from requests_ratelimiter import LimiterSession  # type: ignore  # noqa: F811

        self.session = LimiterSession(
            per_second=0.5,  # at most 1 requests per 2 seconds
            per_minute=12,  # Once every 5 seconds
            bucket_class=SQLiteBucket,
            bucket_kwargs={"path": path},
            per_host=True,
        )

    def get_session(self):
        return self.session


SESSION_PROVIDER = SessionProvider()


def set_cache_directories(dir_path=None):
    global INSTALL_DIR, TOOL_INFO_DOWNLOAD_PATH, CACHE_DIR_PATH

    if dir_path:
        cache_dir = Path(dir_path).resolve()
    elif os.getenv("XDG_CACHE_HOME"):
        cache_dir = Path(os.getenv("XDG_CACHE_HOME")) / "coveriteam"
    else:
        cache_dir = Path.home() / ".cache" / "coveriteam"

    CACHE_DIR_PATH = str(cache_dir.resolve())
    INSTALL_DIR = cache_dir / "tools"
    TOOL_INFO_DOWNLOAD_PATH = cache_dir / "toolinfocache"
    sys.path.append(str(TOOL_INFO_DOWNLOAD_PATH))
    create_cache_directories()


def set_cache_update(flag):
    global UPDATE_CACHE
    UPDATE_CACHE = flag


def set_use_mpi_flag(flag):
    global PORTFOLIO_USE_MPI
    PORTFOLIO_USE_MPI = flag


def create_cache_directories():
    # Create directories and set path.
    if not TOOL_INFO_DOWNLOAD_PATH.is_dir():
        TOOL_INFO_DOWNLOAD_PATH.mkdir(parents=True)

    if not INSTALL_DIR.is_dir():
        INSTALL_DIR.mkdir(parents=True)


def get_INSTALL_DIR():
    return INSTALL_DIR


def get_TOOL_INFO_DOWNLOAD_PATH():
    return TOOL_INFO_DOWNLOAD_PATH


def get_CACHE_DIR_PATH():
    return CACHE_DIR_PATH


def is_CGroupsV1() -> bool:
    return _get_cgroup_version() == CGROUPS_V1


def is_url(path_or_url):
    return "://" in path_or_url or path_or_url.startswith("file:")


def create_archive(dirname, archive_path):
    with ZipFile(archive_path, "w", ZIP_DEFLATED) as zipf:
        for root, _dirs, files in os.walk(dirname):
            for f in files:
                filepath = os.path.join(root, f)
                zipf.write(filepath, os.path.relpath(filepath, dirname))


def filter_dict(d, keys_to_keep):
    """Filters the first dict by the keys given as second parameter."""
    return {k: d[k] for k in d if k in keys_to_keep}


def complete_outputs(
    provided_outputs: Dict[str, "Artifact"],
    expected_outputs: Dict[str, Type["Artifact"]],
) -> Dict[str, "Artifact"]:
    """Returns a dictionary with all expected outputs.
    Outputs not provided by provided_outputs are initialized with
    the neutral version of each artifact type."""
    return {k: provided_outputs[k] if k in provided_outputs else expected_outputs[k]() for k in expected_outputs}


def str_dict(d):
    return {k: str(d[k]) for k in d.keys()}


def artifact_name_clash(
    required_dict: Dict[str, Type["Artifact"]],
    provided_dict: Dict[str, Type["Artifact"]],
    allow_both_directions: bool = False,
) -> bool:
    """This function checks for a name clash in the provided dictionaries

    A name clash exists if a key of the required_dict is also in the provided dict
    and its value in the required_dict is NOT a subtype of the provided value.
    For example:
        if key in required_dict and in provided_dict:
            required_dict[key] => provided_dict[key]

    If allow_both_directions is True the following applies:
        if key in required_dict and in provided_dict:
            required_dict[key] => provided_dict[key]
            OR
            required_dict[key] <= provided_dict[key]

    Args:
        required_dict: Most of the time the input of an actor
        provided_dict: Most of the time provided inputs to a composition
        allow_both_directions: If subclassing should be allowed in both directions

    Returns:
        True, if a name clash exists between the two dictionaries
    """
    for k in required_dict:
        if k in provided_dict and not issubclass(provided_dict[k], required_dict[k]):
            if not allow_both_directions:
                return True
            if not issubclass(required_dict[k], provided_dict[k]):
                return True

    return False


def get_type_per_key_dict_list(type_dicts: List[Dict], selector: Callable[[Type, Type], Type]) -> Dict[str, Type]:
    """Like util.get_type_per_key(), but accepts a list of dictionaries instead of only two"""
    if len(type_dicts) == 1:
        return type_dicts[0]
    if len(type_dicts) == 2:
        return get_type_per_key(type_dicts[0], type_dicts[1], selector)

    return get_type_per_key(
        type_dicts[0],
        get_type_per_key_dict_list(type_dicts[1:], selector),
        selector,
    )


def get_type_per_key(
    type_dict_one: Dict[str, Type],
    type_dict_two: Dict[str, Type],
    selector: Callable[[Type, Type], Type],
) -> Dict[str, Type]:
    """Returns a dictionary of the types selected by the selector.

    The returned dictionary will contain all keys from both dictionaries.
    If a key exists in both dictionaries, its value is defined by the selector.
    If not, the value stays the same

    Args:
        type_dict_one: A dictionary with types as values
        type_dict_two: A dictionary with types as values
        selector: A callable (for example a function), which accepts two types as input and returns a type

    Returns:
        A dictionary with every key from both input dictionaries
    """
    return_dict = {}

    for name, artifact_type in type_dict_one.items():
        if name not in type_dict_two:
            return_dict[name] = type_dict_one[name]
        else:
            return_dict[name] = selector(artifact_type, type_dict_two[name])

    # Add missing values from type_dict_two
    return_dict.update({k: v for k, v in type_dict_two.items() if k not in return_dict})
    return return_dict


def subsumes(left: Dict[str, Type], right: Dict[str, Type]) -> bool:
    """Returns wether left subsumes right. This function acts as custom `<=` implementation
    for type dicts.

    The function will return true if all keys of right are contained in left and
    if for all keys in right the corresponding type is a subclass of the one associated
    with the same key in left.

    Args:
        left: A dictionary with types as values
        right: A dictionary with types as values

    Returns:
        True if left subsumes right, else False
    """

    if not set(right.keys()).issubset(left.keys()):
        return False

    gen_type_comparison = (issubclass(item, left[key]) for (key, item) in right.items())

    return all(gen_type_comparison)


def rename_dict(d, renaming_map):
    return {(renaming_map.get(k, None) or k): d[k] for k in d.keys()}


def collect_variables(exp):
    regex_isinstance = r"(?<=isinstance\()\S+(?=,)"
    regex_in = r"\w+(?= in \[)"
    regex = regex_isinstance + "|" + regex_in
    names = re.findall(regex, exp)

    return names


def get_additional_paths_for_container_config():
    base_dir = Path(__file__).parent.parent.resolve()
    paths = [str(base_dir / "lib"), str(base_dir / "coveriteam" / "toolconfigs")]
    return paths


def specific_type_selector(type1: Type, type2: Type) -> Type:
    """Returns the type, which is a subclass of the other

    For example for
        type1 = Artifact and
        type2 = Verdict
    the returned type is Verdict

    Args:
        type1: A type which should be comparable to type2
        type2: A type which should be comparable to type1

    Returns:
        The more generic type

    Raises:
        ValueError: If one type is not a subtype of the other
    """
    if issubclass(type1, type2):
        return type1

    if issubclass(type2, type1):
        return type2

    raise ValueError("Not possible to compare %s and %s" % (type1, type2))


def generic_type_selector(type1: Type, type2: Type) -> Type:
    """Returns the type, which is a super class of the other

    For example for
        type1 = Artifact and
        type2 = Verdict
    the returned type is Artifact

    Args:
        type1: A type which should be comparable to type2
        type2: A type which should be comparable to type1

    Returns:
        The more generic type

    Raises:
        ValueError: If one type is not a subtype of the other
    """
    if issubclass(type1, type2):
        return type2

    if issubclass(type2, type1):
        return type1

    raise ValueError("Not possible to compare %s and %s" % (type1, type2))
