# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os
from pathlib import Path

import yaml

files_to_delete = set()


class ActorDefinitionLoader(yaml.SafeLoader):
    def __init__(self, stream, file_path):
        super().__init__(stream)
        self.file_path = file_path

    @staticmethod
    def resolve_includes(d):
        # Check if "imports" exist
        imports = d.pop("imports", None)
        if not imports:
            return d

        if not isinstance(imports, list):
            imports = [imports]

        di = {}
        for i in imports:
            imported_file = i.pop("imported_file", None)
            files_to_delete.add(imported_file)
            di = dict_merge(di, ActorDefinitionLoader.resolve_includes(i))
        return dict_merge(di, d)


def dict_merge(d1, d2):
    # This function recursively merges (updates) the dictionaries.
    for k in d2.keys():
        if k in d1.keys():
            if isinstance(d1[k], dict) and isinstance(d2[k], dict):
                d1[k] = dict_merge(d1[k], d2[k])
            elif isinstance(d1[k], dict) or isinstance(d2[k], dict):
                msg = "YAML file could not be parsed. Clash in the tag: %r" % k
                raise Exception(msg)
            d1[k] = d2[k]
        else:
            d1[k] = d2[k]

    return d1


def include_constructor(loader, node):
    original_file_path = os.path.dirname(loader.file_path)
    include_file_path = Path(original_file_path + "/" + node.value)
    with include_file_path.open("r") as file:
        loader = ActorDefinitionLoader(file, include_file_path)
        content = loader.get_single_data()
        return dict_merge(content, {"imported_file": include_file_path})


yaml.SafeLoader.add_constructor("!include", include_constructor)


def change_format(file_path):
    with open(file_path, "r") as file:
        loader = ActorDefinitionLoader(file, file_path)
        content = loader.get_single_data()
        content = loader.resolve_includes(content)

    if "actor_name" in content:
        content["name"] = content.pop("actor_name")
    if "toolinfo_module" in content:
        content["benchexec_toolinfo_module"] = content.pop("toolinfo_module")
    if "format_version" in content:
        content["format_version"] = "2.0"
    if "archives" in content:
        content["versions"] = content.pop("archives")
        for version in content["versions"]:
            if "location" in version:
                version["url"] = version.pop("location")
            if "options" in version:
                version["benchexec_toolinfo_options"] = version.pop("options")
    if "options" in content:
        content["benchexec_toolinfo_options"] = content.pop("options")
        if "versions" in content:
            for version in content["versions"]:
                version["benchexec_toolinfo_options"] = content["benchexec_toolinfo_options"]

    with open(file_path, "w") as file:
        yaml.safe_dump(content, file)


def process_directory(directory):
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".yml"):
                file_path = os.path.join(root, file)
                change_format(file_path)


def delete_included_files():
    for file in files_to_delete:
        try:
            file.unlink()
            print(f"Deleted file: {file}")
        except FileNotFoundError:
            pass
        except Exception as e:
            print(f"Error deleting file {file}: {e}")


def main():
    actors = "./actors/"
    examples = "./examples/"

    process_directory(actors)
    process_directory(examples)

    delete_included_files()


if __name__ == "__main__":
    main()
