#!/bin/bash

# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# TODO still 1 issue pending:
# steps for packing:
#   - clone coveriteam
#     - copy coveriteam, bin, lib
#     - copy yml and cvt file, also resource yml file
#     - prepare the tools directory: copy cst_transform, download tools and unzip
#   - maybe have a list of verifiers somewhere.
#     This would be used for: labelling as well as downloading tools

# TODO how to we deal with licenses of the zipped tools?

set -eao pipefail
# IFS=$'\t\n'

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
DIR_COVERITEAM="coveriteam"

# Create temp directory and push it
TMPDIR=$(mktemp -d)
# TODO update this variable. This should be the name of the tool, as in the benchdef.
TOZIP="validating-portfolio"
pushd "$TMPDIR" > /dev/null
mkdir $TOZIP

# Clone coveriteam
git clone $SCRIPT_DIR/../../../coveriteam
pushd $DIR_COVERITEAM

git checkout main

popd

# Start adding the files to the directory to be zipped.
# Copy coveriteam, bin, lib, and licenses.
cd $DIR_COVERITEAM
cd ..
cp -r $DIR_COVERITEAM/{bin,coveriteam,lib,LICENSE,LICENSES} "$TOZIP/"
# Add the cvt file.
# TODO It doesn't need to be copied in the examples folder.
# TODO Maybe we can put the cvt file in the contrib along with this script? 

cp $SCRIPT_DIR/verifier+validator-portfolio.cvt "$TOZIP/"

# Copy required actor definitions yml.

cp -r "$SCRIPT_DIR/actors/" "$TOZIP"


zip -r "$TOZIP.zip" "$TOZIP"
popd
mv "$TMPDIR/$TOZIP.zip" ./
rm -rf $TMPDIR
