#!/bin/bash

# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -eao pipefail
# IFS=$'\t\n'

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
DIR_COVERITEAM="coveriteam"
# Don't want to put cst_tranform in verifiers as it is not a verifier.
VERIFIERS="2ls cbmc cpa-seq divine esbmc-kind goblint pesco symbiotic uautomizer ukojak utaipan"

# Create temp directory and push it
TMPDIR=$(mktemp -d)
TOZIP="coveriteam-verifiers"
pushd "$TMPDIR" > /dev/null
mkdir $TOZIP

# Clone coveriteam
git clone https://gitlab.com/sosy-lab/software/coveriteam.git
pushd $DIR_COVERITEAM

git checkout svcomp22-prepare-archive
# temp solution till we merge it in main
cp -r $SCRIPT_DIR "contrib/"

popd

# Start adding the files to the directory to be zipped.

# Copy coveriteam, bin, lib, and licenses.
cp -r $DIR_COVERITEAM/{bin,coveriteam,lib,LICENSE,LICENSES} "$TOZIP/"
cp -r $SCRIPT_DIR/{actors,*.cvt,README} "$TOZIP/"

pushd "$TOZIP/"

# Download and install the actors:
CACHE_DIR="cache"
for verifier in $VERIFIERS;
  do
    ./bin/coveriteam --cache-dir $CACHE_DIR "actors/$verifier.yml" --test-tool
done

# for the cst_transform
./bin/coveriteam --cache-dir $CACHE_DIR "actors/feature-extractor.yml" --test-tool

# Delete the zip archives. It reduces the size of the archive.
rm $CACHE_DIR/archives/*
# Need to create some file, as vcloud does not trasnfer an empty dir. 
touch $CACHE_DIR/archives/dummy

popd

zip -r "$TOZIP.zip" "$TOZIP"
popd
mv "$TMPDIR/$TOZIP.zip" ./coveriteam-verifier-algo-selection.zip
rm -rf $TMPDIR
