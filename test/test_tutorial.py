# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path

import pytest
from test_util import extract_verdict_from_output

from coveriteam.coveriteam import CoVeriTeam

EXAMPLES = Path(__file__).parent.parent / "examples"


@pytest.fixture
def setup_cache():
    return ["--cache-dir", str(Path(__file__).parent.parent / "cache")]


def in_examples(path: str) -> Path:
    return str(EXAMPLES / path)


def argument(key, path) -> str:
    is_path = {"actordef_dir"}
    if "path" in key or key in is_path:
        return f"{key}={in_examples(path)}"
    return f"{key}={path}"


def test_tutorial_verifier(setup_cache):
    inputs = [in_examples("verifier-C.cvt")]
    inputs += ["--input", argument("verifier_path", "../actors/cpachecker.yml")]
    inputs += ["--input", argument("program_path", "test-data/c/Problem02_label16.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += ["--input", argument("verifier_version", "default")]
    inputs += ["--input", argument("data_model", "ILP32")]

    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_tutorial_validating_verifier(setup_cache):
    inputs = [in_examples("validating-verifier.cvt")]
    inputs += ["--input", argument("program_path", "test-data/c/Problem02_label16.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += [
        "--input",
        argument("validator_path", "../fm-tools/data/cpachecker.yml"),
    ]
    inputs += ["--input", argument("validator_version", "svcomp24-violation")]
    inputs += ["--input", argument("verifier_path", "../actors/cpachecker.yml")]
    inputs += ["--input", argument("verifier_version", "default")]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_execution_based_validation(setup_cache):
    inputs = [in_examples("execution-based-validation.cvt")]
    inputs += ["--input", argument("program_path", "test-data/c/Problem01_label15.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += [
        "--input",
        argument("witness_path", "test-data/witnesses/Problem01_label15_reach_safety.graphml"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


@pytest.mark.skip
def test_execution_based_validation_witness_instrument(setup_cache):
    inputs = [in_examples("exe-validator-witness-instrument.cvt")]
    inputs += ["--input", argument("program_path", "test-data/c/gcnr2008.i")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += [
        "--input",
        argument("witness_path", "test-data/witnesses/gcnr2008_violation_witness.graphml"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_cmc_reducer(setup_cache):
    inputs = [in_examples("reducer-based-conditional-model-checker.cvt")]
    inputs += [
        "--input",
        argument("program_path", "test-data/c/slicingReducer-example.c"),
    ]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += ["--input", argument("cond_path", "test-data/c/slicingCondition.txt")]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_condtest(setup_cache):
    inputs = [in_examples("CondTest/condtest.cvt")]
    inputs += ["--gen-code"]
    inputs += ["--input", argument("program_path", "test-data/c/test.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/coverage-branches.prp"),
    ]
    inputs += ["--input", argument("tester_yml", "../actors/klee.yml")]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_verifier_based_tester(setup_cache):
    inputs = [in_examples("verifier-based-tester.cvt")]
    inputs += ["--input", argument("program_path", "test-data/c/CostasArray-10.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


@pytest.mark.skip
def test_repeat_condtest(setup_cache):
    inputs = [in_examples("CondTest/repeat-condtest.cvt")]
    inputs += ["--input", argument("program_path", "test-data/c/Problem01_label15.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/coverage-branches.prp"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_metaval(setup_cache):
    inputs = [in_examples("MetaVal/metaval.cvt")]
    inputs += [
        "--input",
        argument("program_path", "test-data/c/ConversionToSignedInt.i"),
    ]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/no-overflow.prp"),
    ]
    inputs += [
        "--input",
        argument(
            "witness_path",
            "test-data/witnesses/ConversionToSignedInt_nooverflow_witness.graphml",
        ),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += setup_cache
    CoVeriTeam().start(inputs)


def test_multi_sequence_fase_22(setup_cache):
    inputs = ["--input", argument("program_path", "test-data/c/Problem02_label16.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]

    expected_verdict = extract_verdict_from_output(
        CoVeriTeam().start, [str(EXAMPLES / "FASE22" / "sequence-8.cvt")] + inputs
    )
    verdict = extract_verdict_from_output(
        CoVeriTeam().start,
        [str(EXAMPLES / "FASE22" / "sequence-8-with-new-sequence.cvt")] + inputs,
    )
    assert expected_verdict == verdict


@pytest.mark.skip
def test_multi_parallel_composition_fase_22(setup_cache):
    # Testing the construction of parallel composition with multiple arguments.
    inputs = ["--input", argument("program_path", "test-data/c/Problem02_label16.c")]
    inputs += [
        "--input",
        argument("specification_path", "test-data/properties/unreach-call.prp"),
    ]
    inputs += ["--input", argument("data_model", "ILP32")]
    inputs += ["--input", argument("actordef_dir", "FASE22/actor-definitions")]

    expected_verdict = extract_verdict_from_output(
        CoVeriTeam().start, [str(EXAMPLES / "FASE22" / "cst-selection-8.cvt")] + inputs
    )
    verdict = extract_verdict_from_output(
        CoVeriTeam().start,
        [str(EXAMPLES / "FASE22" / "cst-selection-8-with-new-parallel.cvt")] + inputs,
    )
    assert expected_verdict == verdict
