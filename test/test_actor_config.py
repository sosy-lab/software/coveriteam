# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os
from pathlib import Path
from typing import List

# If this import id missing, then the call `benchexec.util.<func>` will
# fail. This may be caused by coveriteams import magic.
import benchexec.util  # noqa F401
from pytest import raises  # noqa PT013

from coveriteam import coveriteam
from coveriteam.language import CoVeriLangException
from coveriteam.language.actorconfig import (
    dict_merge,
)

all_yaml_files: List[Path]

files_to_skip = {
    "cpachecker-BASE.yml",
    "condtest.yml",
    "cst-transform.yml",
    "verifier_resource.yml",
    "verifier+validator-portfolio.yml",
}


def setup_module():
    global all_yaml_files
    coveriteam.util.set_cache_directories()
    coveriteam.util.set_cache_update(False)
    os.chdir(Path(os.path.realpath(__file__)).parent.parent)
    actor_path = Path(os.getcwd()) / "actors"
    all_yaml_files = [
        (actor_path / file_string).resolve()
        for file_string in os.listdir(str(actor_path))
        if (actor_path / file_string).resolve().suffix == ".yml"
    ]


def test_dict_merge():
    d1 = {"a": "b", "b": "b", "c": "d"}
    d2 = {"c": "e"}
    d3 = {"dict": d2}
    d4 = d2.copy()
    d4["dict"] = "no_dict"

    assert dict_merge(d1, d2) == {"a": "b", "b": "b", "c": "e"}
    assert dict_merge(d2, d3) == {"c": "e", "dict": d2}

    # Should throw an exception with the current solution
    with raises(CoVeriLangException):
        dict_merge(d4, d2)
