# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os
from pathlib import Path
from typing import List

import benchexec.util  # noqa F401
import requests

from coveriteam import coveriteam
from coveriteam.language.actorconfig import (
    ActorConfig,
)

all_yaml_files: List[Path]

files_to_skip = {
    "cpachecker-BASE.yml",
    "condtest.yml",
    "cst-transform.yml",
    "verifier_resource.yml",
    "verifier+validator-portfolio.yml",
}


def setup_module():
    global all_yaml_files
    coveriteam.util.set_cache_directories()
    coveriteam.util.set_cache_update(False)
    os.chdir(Path(os.path.realpath(__file__)).parent.parent)
    actor_path = Path(os.getcwd()) / "actors"
    all_yaml_files = [
        (actor_path / file_string).resolve()
        for file_string in os.listdir(str(actor_path))
        if (actor_path / file_string).resolve().suffix == ".yml"
    ]


class _TestActorConfig(ActorConfig):
    """Prints the file name of this actor for the generated tests"""

    def __repr__(self) -> str:
        return str(self.path.name)


def test_validity_of_download_url():
    global all_yaml_files

    urls = set()

    for file in all_yaml_files:
        config = ActorConfig.read_yaml(file)
        if file.name in files_to_skip:
            continue
        if "versions" not in config or not config["versions"]:
            continue
        for archive in config["versions"]:
            version = archive["version"]
            actor_config = _TestActorConfig(file, version)
            try:
                urls.add(actor_config.archive_location)
            except AttributeError:
                {check_download_url(elem) for elem in urls}
                return


def check_download_url(url: str):
    """Checks, if the given url is valid by retrieving only the headers"""
    answer: requests.Response = requests.head(url=url, timeout=10)
    assert answer.status_code == 200, "Error %s while checking %s" % (
        answer.status_code,
        url,
    )
