# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import io
import sys
from argparse import Namespace

import pytest

from coveriteam.coveriteam import CoVeriTeam
from coveriteam.util import BASE_CONFIG, Config

dummy_args_dict = {
    "test_attr": "value",
}
dummy_args = Namespace(**dummy_args_dict)


def test_init_config():
    configuration_1 = Config()
    configuration_2 = Config()
    configuration_2.load(args=dummy_args)

    assert configuration_1 == configuration_2
    assert "test_attr" in configuration_1._config["defaults"]


def test_update_config():
    existing_values = {"test_attr": 123}
    non_existing_values = {"fake_attr": "value"}

    configuration = Config()
    configuration.load(args=dummy_args)
    configuration.update(values_to_update=existing_values)
    configuration.update(values_to_update=non_existing_values)

    assert configuration._config["defaults"]["test_attr"] == 123
    assert "fake_attr" not in configuration._config["defaults"]


def test_dump_config():
    output_buffer = io.StringIO()
    sys.stdout = output_buffer

    try:
        with pytest.raises(SystemExit) as _:
            CoVeriTeam().create_argument_parser().parse_args(["--dump-default-config"])

        output = output_buffer.getvalue().strip()
        assert output == BASE_CONFIG.strip()
    finally:
        sys.stdout = sys.__stdout__
