# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
import pytest

from coveriteam.language.artifact import Artifact, CProgram, Verdict, Witness
from coveriteam.util import subsumes

pmp = pytest.mark.parametrize


def test_get_type_per_key():
    pass


def test_get_type_per_key_dict_list():
    pass


@pmp(
    ["left", "right", "expected"],
    [
        ({"verdict": Artifact, "program": CProgram}, {"verdict": Verdict}, True),
        ({"verdict": Artifact, "program": CProgram}, {"verdict": Artifact}, True),
        ({"verdict": Artifact, "program": CProgram}, {"verdict": Witness}, False),
        (
            {"verdict": Artifact, "program": CProgram},
            {"verdict": Verdict, "test_suite": Witness},
            False,
        ),
    ],
)
def test_subsumes(left, right, expected):
    print(subsumes(left, right) == expected)
