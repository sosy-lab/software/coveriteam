#! /bin/bash

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# This script will just call the tools without any argument. It will download the tool archives if required.

for conf in actors/*.yml;
do
  echo "testing tool in the actor config: $conf"
  bin/coveriteam --test-tool $conf
done

